from datetime import date
import pandas as pd
import yfinance as yf

def calculate_sma(ticker, start_date, end_date, window):
    stock = yf.Ticker(ticker)
    historical_data = stock.history(start=start_date, end=end_date)
    close_prices = historical_data['Close']
    sma = close_prices.rolling(window=window).mean()
    return sma

# ticker list from file
ticker_list = []
with open('stockList.txt', encoding="utf-8") as f:
    ticker_list = f.read().splitlines()
print(ticker_list)

# Historical data from one year's time
end_date = date.today() 
start_date = date(end_date.year - 1, 
                           end_date.month, 
                           end_date.day)
window_7 = 7
window_21 = 21

# Create a DataFrame to store the SMA and ratio data
data_list = []
for t in ticker_list:
    sma_7 = calculate_sma(t, start_date, end_date, window_7)
    sma_21 = calculate_sma(t, start_date, end_date, window_21)

    sma_ratio = (sma_7 - sma_21) / sma_21

    # Max, Min and dates of sma ratios
    data_row = {'Ticker' : t,
                'sma ratio ajd' : sma_ratio.values[-1],
                'Max sma ratio date' : sma_ratio.idxmax().date(), 
                'Max sma ratio' : sma_ratio.max(),
                'Min sma ratio date' : sma_ratio.idxmin().date(),
                'Min sma ratio' : sma_ratio.min()}
    data_list.append(data_row)

df = pd.DataFrame(data_list)

# Save the data to a CSV file
df.to_csv('sma_data.csv', index=False)
