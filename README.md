# Retrieve historical stock prices.

Script is called by giving a date interval for stock prices specified by the start and end date in the yyyy-mm-dd format.

Simply specify stocks which you want the historical prices from to the file stockList.txt.

Outputs are written to a CSV file named historicalStockPrices.csv.
