import pandas as pd
import sys
import yfinance as yf

def retrieve_historical_prices(ticker_symbol, start_date, end_date):
    # Retrieve historical data from Yahoo Finance
    data = yf.download(ticker_symbol, start=start_date, end=end_date)
    
    # Return the "Close" prices for each date
    return data['Close']

def writeHistoricalPrices(data):
    df = pd.DataFrame(data)
    df.to_csv('historicalStockPrices.csv')

# Retrieve tickers from file
ticker_list = []
with open('stockList.txt', encoding="utf-8") as f:
    ticker_list.append(f.read())

if __name__ == "__main__":
    for t in ticker_list:
        historical_prices = retrieve_historical_prices(t, 
                                                       sys.argv[1], 
                                                       sys.argv[2])
        print(historical_prices)
    writeHistoricalPrices(historical_prices)

